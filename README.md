AWS (Push Variante)
===================

Umgebung für eine Klasse in AWS Academy

Umgebung Zusammenstellen
------------------------

Vorlage holen und diese um lokales Modul erweitern

    git clone https://github.com/tbz-it/m122
    cd m122

Modul holen

    git clone https://github.com/mc-b/terraform-lerncloud-aws

`main.tf` ändern, damit die Anweisungen vom lokalen Modul (aws) geholt werden.

Gleizeitig können wir die Anzahl VMs die erstellt werden sollen als `count` eintragen.

    module "lerncloud" {
    source     = "./terraform-lerncloud-aws"
    
    module     = "m122"
    count      = 24
    description = "M122 - Automatisieren mit Skripten"
        
    userdata    = "cloud-init.yaml"
    }

Der Name `module =` aller VMs wäre jetzt immer gleich, deshalb diesen um den aktuellen Index erweitern:

    module      = "m122-${format("%02d", count.index+1)}-${terraform.workspace}"

Mittels Anhängen des Workspaces von Terraform, können wir den VM Namen z.B. einfach um den Klassennamen ergänzen.

`terraform init` bringt aber noch zwei Fehler
* Provider ...
* `output.tf`

Die `output.tf` ist nur für einen VM ausgelegt. Mittels Einfügen von '*', vor dem Namen, werden alle IPs ausgegeben. Die restlichen Einträge sind zu löschen.

    output "ip_vm" {
    value = module.lerncloud.*.ip_vm
    description = "The IP address of the server instance."
    }

    output "fqdn_vm" {
    value = module.lerncloud.*.fqdn_vm
    description = "The FQDN of the server instance."
    }
    
    output "description" {
    value       = module.lerncloud.*.description
    description = "Description VM"
    } 

Der AWS Provider bzw. dessen Informationen befindet sich im Modul Verzeichnis und kann einfach von dort in das aktuelle Verzeichnis verschoben werden:

    mv terraform-lerncloud-aws/provider.tf .

Jetzt sollte `terraform init` funktionieren`.

AWS Zugriffsinformationen
-------------------------

Die AWS Zugriffinformationen finden wir in der AWS Academy, wenn das Lab gestartet wurde unter `AWS Details`.

Diese Informationen, z.B. legen wir in einer Datei `config.txt`, im aktuellen Verzeichnis ab und setzen die Umgebungvariable `AWS_CONFIG_FILE`.

`config.txt`

    [default]
    aws_access_key_id=ASIA2YRP5UD5RTKJCRP6
    aws_secret_access_key=Yqoz0jucKYB0QRk5ouXbqFbDn0/UXwJ/QOSpVfJh
    aws_session_token=...

Umgebungsvariable `AWS_CONFIG_FILE`

    export AWS_CONFIG_FILE=config.txt

Testen können wir mit 

    terraform plan

Jetzt sollten 48 Ressourcen (24 Securitygroups und 24 VMs) angezeigt werden. Jetzt müssen wir diese nur noch anlegen:

    terraform apply

Nach 4 Stunden werden die VMs automatisch heruntergefahren, bzw. das Lab beendet. Dies Zeit kann durch Mehrmaliges drücken von `Start Lab` verlängert werden.

Sind die VMs einmal heruntergefahren, ändert sich beim hochfahren (`Start Lab`) deren IPs. Die neuen IPs können wie folgt ausgegeben werden:

    terraform refresh

    

