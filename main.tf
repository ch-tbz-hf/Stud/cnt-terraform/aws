module "lerncloud" {
  source = "./terraform-lerncloud-aws"

  module      = "m122-${format("%02d", count.index + 1)}-${terraform.workspace}"
  count       = 24
  description = "M122 - Automatisieren mit Skripten"

  userdata = "cloud-init.yaml"
}
